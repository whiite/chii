require('dotenv').config();

const Bot = require('./bot');
const Server = require('./server');
const config = require('./config');

const chii = new Bot(config.bot.name);
const server = new Server(config.server.port);

// start http server
server.start();

// okinasai chii
chii.boot();
