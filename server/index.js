const express = require('express');

class Server {
    constructor(port) {
        this.configured = false;
        this.port = port || 80;
        this.app = express();

        this.config();
    }

    config() {
        if (this.configured) {
            return;
        }

        this.configured = true;

        this.app.get('/', (req, res) => {
            res.send('work it!');
        });
    }

    start() {
        this.app.listen(this.port, () => console.log('server started'));
    }
}

module.exports = Server;
