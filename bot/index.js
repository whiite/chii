const Discord = require('discord.js');
const config = require('../config');

class Bot {
    constructor(name) {
        this.name = name;
        this.client = new Discord.Client();

        this.client.on('message', (message) => {
            const { author, content } = message;
            const args = content.slice(config.bot.prefix.length).split(/ +/);
            const command = args.shift().toLowerCase();

            if (!content.startsWith(config.bot.prefix) || author.bot) {
                return;
            }

            if (command === 'hello') {
                message.reply('hola >,<');
            }
        });
    }

    async boot() {
        await this.client.login(config.bot.token);
        console.log('chii booted!');
    }
}

module.exports = Bot;
