const config = {
    bot: {
        name: process.env.BOT_NAME,
        prefix: process.env.BOT_PREFIX,
        token: process.env.BOT_TOKEN,
        permissions: process.env.BOT_PERMISSION_INT
    },
    server: {
        port: process.env.SERVER_PORT
    },
};

Object.freeze(config);

module.exports = config;
